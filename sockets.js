/////////////////////////////////////////////


var io = require('socket.io-client');
var socket = io('http://192.168.2.100:9600'); 
var http = require('http');

socket.on('connect', function () { 
   console.log('Connected to NFC server');
   socket.emit("ADD_PC", {"application" : "Test"});
});

socket.on('error', function(e){
	console.log("****Error*****");
	console.log(e);
});

 socket.on('disconnect', function () { 
   console.log('disconnected to NFC server');
});

socket.on('tagON_192.168.2.8', function (data) { 
    console.log(data);
    var nfctagid = (data)
    console.log(data.tagId);
   });


//////////////////Local Sockets

var local_io = require('socket.io').listen(3000);


local_io.on('connection', function (local_socket) {

  console.log("Received Connection");

  local_socket.on('Connect_Me', function(data){
    console.log("Got Connect Me from interface");
  });

  local_socket.on('Button_Click', function(data){
    console.log("Clicked the button");
    console.log(data);
  });


});
