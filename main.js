const electron = require('electron');
var app = electron.app;
var BrowserWindow = electron.BrowserWindow;

var server = require('./sockets.js');

var mainWindow = null;


app.on('window-all-closed', function() {
 
  if (process.platform != 'darwin') {
    app.quit();
  }
});


app.on('ready', function() {

  mainWindow = new BrowserWindow({
    width: 800,
    height: 800,
    x :0,
    y : 0,
    resizable :true,
    autoHideMenuBar: true, 
    frame: false
  });

 
  /*var screen = require('screen');   
  var size = screen.getPrimaryDisplay().workAreaSize;     

    mainWindow.setBounds({
      x: 0,     
      y: 0,     
      width: size.width * 2,     
      height: size.height,   
    })*/


  mainWindow.loadURL('file://' + __dirname + '/index.html');

  mainWindow.webContents.openDevTools()
  
  mainWindow.on('closed', function() {
    mainWindow = null;
  });
});


